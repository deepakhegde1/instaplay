import { loginData, LogingResponse } from "@/model/Login";
import axios from "axios";
import { urlContainer } from "../urlContainer";

export const requestTokenProcess = async (): Promise<LogingResponse> => {
  const response = await axios.get(urlContainer.requestToken);
  return response?.data;
};

export const loginProcess = async (
  data: loginData
): Promise<LogingResponse> => {
  const response = await axios.post(urlContainer.login, data);
  return response?.data;
};
