import { notification } from "antd";

export const getToken = (name: string): string | null => {
  const token: string | null = localStorage.getItem(name);
  return token;
};

export const setToken = (token?: any): void => {
  localStorage.setItem("token", token);
};

export const isAuthenticated = () => {};

export const notify = (msg: string): any => {
  notification["success"]({
    message: msg,
    placement: "bottomLeft",
  });
};
export const notifyErr = (msg: string): any => {
  notification["error"]({
    message: msg,
    placement: "bottomLeft",
  });
};
